﻿## Cài đặt

Yêu cầu: **jQuery, FontAwesome**.

Clone về và nhúng hai file sau vào website của bạn.
```markdown
<link href="x-vermenu.min.css" rel="stylesheet" />
```

```markdown
<script src="x-vermenu.min.js"></script>
```

## Sử dụng

1. Tạo một thẻ với ID là `x-verMenu`

```markdown
<div id="x-verMenu"></div>
```

2. Chèn vào trong nội dung menu theo cấu trúc sau:

```markdown
<div id="x-verMenu">
 <a href="#" class="toggle">Tên menu</a>
 <ul>
  <li><a href="#">item 1</a></li>
  <li><a href="#">item 2</a></li>
  <li><a href="#">item 3</a></li>
 </ul>
</div>
```

3. Sử dụng class `dropdown` lên các thẻ `<li>` nào chứa menu con bên trong.

```markdown
<div id="x-verMenu">
 <a href="#" class="toggle">Tên menu</a>
 <ul>
  <li class="dropdown">
  <a href="#">item 1</a>
   <ul>
    <li><a href="#">sub item 1</a></li>
    <li><a href="#">sub item 2</a></li>
   </ul>
  </li>
  <li><a href="#">item 2</a></li>
  <li><a href="#">item 3</a></li>
 </ul>
</div>
```
﻿// menu toggle
$(function () {
	var toggle = $('#x-verMenu > .toggle'),
      ul = $('#x-verMenu > ul'),
			dropdown = $('#x-verMenu .dropdown');

	// toggle
	toggle.on('click', function (e) {
		e.preventDefault();
		ul.toggleClass('visible');
	});

	// Nav dropdown toggle
	dropdown.on('click', function () {
		var $this = $(this);
		$this.children().children().removeClass('show');
		$this.toggleClass('show').siblings().removeClass('show');

		if ($this.children('ul').hasClass('open')) {
			$this.children('ul').removeClass('open');
			$this.children('ul').slideUp(350);
		}
		else {
			$this.parent().parent().find('li ul').removeClass('open');
			$this.parent().parent().find('li ul').slideUp(350);
			$this.children('ul').toggleClass('open');
			$this.children('ul').slideToggle(350);
		}
	});

	// Prevent click events from firing on children of navDropdownToggle
	dropdown.on('click', '*', function (e) {
		e.stopPropagation();
	});
});

